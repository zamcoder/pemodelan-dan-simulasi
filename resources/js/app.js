require('./bootstrap');
import 'flowbite';
import Datepicker from '@themesberg/tailwind-datepicker/Datepicker';
import DateRangePicker from '@themesberg/tailwind-datepicker/DateRangePicker';
import Alpine from 'alpinejs';

window.Alpine = Alpine;
window.Datepicker = Datepicker;
window.DateRangePicker = DateRangePicker;

Alpine.start();