<div>
    <form method="POST">
        @csrf
        <div class="grid grid-cols-12 w-100 gap-4 mx-auto mb-3">
            <div class="col-span-6">
                <label for="sisiAB" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Sisi AB</label>
                <input type="number" wire:model.defer="sisiAB" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-violet-500 focus:border-violet-500 block w-full p-2.5" placeholder="Ex: 9" required>
                @error('sisiAB') <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p> @enderror
            </div>
            <div class="col-span-6">
                <label for="sisiBC" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Sisi BC</label>
                <input type="number" wire:model.defer="sisiBC" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-violet-500 focus:border-violet-500 block w-full p-2.5" placeholder="Ex: 11" required>
                @error('sisiBC') <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p> @enderror
            </div>
            <div class="col-span-6">
                <label for="sisiCD" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Sisi CD</label>
                <input type="number" wire:model.defer="sisiCD" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-violet-500 focus:border-violet-500 block w-full p-2.5" placeholder="Ex: 6" required>
                @error('sisiCD') <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p> @enderror
            </div>
            <div class="col-span-6">
                <label for="sisiDA" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Sisi DA</label>
                <input type="number" wire:model.defer="sisiDA" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-violet-500 focus:border-violet-500 block w-full p-2.5" placeholder="Ex: 8" required>
                @error('sisiDA') <p class="mt-2 text-sm text-red-600 dark:text-red-500">{{ $message }}</p> @enderror
            </div>
            <div class="col-span-12">
                <button type="button" wire:click.prevent="storePost()" class="w-full text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>
