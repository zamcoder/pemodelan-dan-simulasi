<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Str;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class P02 extends Component
{
    use LivewireAlert;

    public $sisiAB, $sisiBC, $sisiCD, $sisiDA;

    protected $rules = [
        'sisiAB' => 'required',
        'sisiBC' => 'required',
        'sisiCD' => 'required',
        'sisiDA' => 'required',
    ];

    protected $messages = [
        'sisiAB.required' => 'Sisi AB wajib dipilih.',
        'sisiBC.required' => 'Sisi BC wajib diisi.',
        'sisiCD.required' => 'Sisi CD wajib dipilih.',
        'sisiDA.required' => 'Sisi DA wajib diisi.',
    ];

    public function storePost()
    {
        $this->validate();

        $data = array($this->sisiAB,$this->sisiBC,$this->sisiCD,$this->sisiDA);
        sort($data);

        $a = pow($data[0], 2)+pow($data[1], 2);
        $b = pow($data[2]+$data[3], 2);

        if($a < $b){
            $luasA = 0.5*$data[0]*$data[1];
            $semiperimeter = 0.5*(sqrt($a)+$data[2]+$data[3]);
            $luasB = sqrt($semiperimeter*($semiperimeter-sqrt($a))*($semiperimeter-$data[2])*($semiperimeter-$data[3]));

            $updated = $luasA+$luasB;
        }
        else{
            $a = sqrt(pow($this->sisiAB, 2)+pow($this->sisiDA, 2));
            $alas = 0.5*$a;

            $ax = sqrt(pow($this->sisiAB, 2)-pow($alas, 2));
            $cx = sqrt(pow($this->sisiBC, 2)-pow($alas, 2));

            $luasABX = 0.5*$alas*$ax;
            $luasBCX = 0.5*$alas*$cx;
            $luasCDX = 0.5*$alas*$cx;
            $luasDAX = 0.5*$alas*$ax;

            $updated = $luasABX+$luasBCX+$luasCDX+$luasDAX;
        }

        if($updated){
            $this->alert('success', 'Hasilnya adalah = '.$updated);
        }
        else{
            $this->alert('error', 'Ada Kesalahan Dalam Data.');
        }
    }

    public function render()
    {
        return view('livewire.p02');
    }
}
